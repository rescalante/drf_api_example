from rest_framework import serializers
from accounts.models import UserProfile, Subscription, Country
from payments.models import Invoice
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

#Django User Serializer
class UserSerializer(serializers.Serializer):
    """
    Simple Serializer based on the Django User model
    * Can be modified to serialize more data or use another model
    """
    #username = serializers.CharField(max_length=20)
    email = serializers.CharField()
    #password = serializers.CharField(max_length=20)

    def create(self, validated_data):
        """
        Creates an instance of a User
        """
        return User.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.password = validated_data.get('password', instance.password)

        instance.save()
        return instance

#UserProfile Serializer
class UserProfileSerializer(serializers.ModelSerializer):
    user_email = serializers.EmailField(source='user.email', read_only=True)

    class Meta:
        model = UserProfile
        fields = ('id', 'user_email', 'gender', 'phone','birth_date', 'user_type', 'language_code')


#Subscription Serializers
class ModelSubscriptionSerializer(serializers.ModelSerializer):
    #UserProfileSerializer Instance to bring user info to this serializer
    user = UserProfileSerializer()

    class Meta:
        model = Subscription
        fields = ('user', 'status', 'start_date', 'end_date')

#Invoices serializers
class ModelInvoiceSerializer(serializers.ModelSerializer):
    #UserProfileSerializer Instance to bring user info to this serializer
    user = UserProfileSerializer()

    class Meta:
        model = Invoice
        fields = ('user', 'status', 'invoice_number', 'created_date')
