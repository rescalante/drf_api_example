# coding=utf-8
import accounts.models
from accounts.models import UserProfile, Subscription
from payments.models import Invoice
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework import authentication, permissions, serializers, viewsets, status, filters
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserSerializer, UserProfileSerializer, ModelSubscriptionSerializer, ModelInvoiceSerializer
from .filters import SubscriptionFilter, InvoiceFilter, UserProfileFilter

# Create your views here.
class ListUsers(APIView):
    """
    View to list the users registered on the database.
    * Requires token authentication.
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request):
        """
        Return a list of serialized data for all the users registered on the database
        """
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({"usuarios": serializer.data})


class CreateUser(APIView):
    """
    View to create a single user via post
    * Requires token authentication.
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request):
        user = {
            'username': request.data.get('username'),
            'email': request.data.get('email'),
            'password': request.data.get('password')
        }
        serializer = UserSerializer(data=user)
        if serializer.is_valid():
            user_saved = serializer.save()
            return Response({"success": "Usuario '{}' creado exitosamente".format(user_saved.username)}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ReadUser(APIView):
    """
    View to get the detail of a single user
    * Requires token authentication.
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request, pk):
        user = get_object_or_404(User.objects.all(), pk=pk) #Obtain user with id=pk
        serializer = UserSerializer(user)
        return Response({"usuarios": serializer.data})

class UpdateUser(APIView):
    """
    View to update data from a single user
    * Requires token authentication.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    
    def put(self, request, pk):
        saved_user = get_object_or_404(User.objects.all(), pk=pk)
        user = {
            'username': request.data.get('username'),
            'email': request.data.get('email'),
            'password': request.data.get('password')
        }
        serializer = UserSerializer(instance=saved_user, data=user, partial=True)
        if serializer.is_valid():
            user_saved = serializer.save()
            return Response({"success": "Usuario '{}' actualizado exitosamente".format(user_saved.username)}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DeleteUser(APIView):
    """
    View to delete a single user
    * Requires token authentication.
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def delete(self, request, pk):
        # Get object with this pk
        user = get_object_or_404(User.objects.all(), pk=pk)
        user.delete()
        return Response({"message": "Usuario con id `{}` ha sido borrado.".format(pk)}, status=status.HTTP_204_NO_CONTENT)

#### Classes for User and Subscriptions related methods ###
class UserStatus(APIView):
    """
    View to get the status of an user given the user_id
    *Requires token authentication
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request, pk):
        user = get_object_or_404(UserProfile.objects.all(), pk=pk) #Obtain user with id=pk
        serializer = UserProfileSerializer(user)
        return Response({"usuario": serializer.data})

class UserSubscription(APIView):
    """
    View to get the information about a subscription of a 
    certain user
    *Requires token authentication
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request, pk):
        params = self.request.query_params
        subscriptions = Subscription.objects.filter(user=pk)
        filtered_subs = SubscriptionFilter(params, subscriptions)
        serializer = ModelSubscriptionSerializer(filtered_subs.qs, many=True)
        return Response({"subscripciones":serializer.data})

class AllSubscriptions(APIView):
    """
    View to get the information about all subscriptions
    *Requires token authentication
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request):
        params = self.request.query_params
        subscriptions = Subscription.objects.all()
        filtered_subs = SubscriptionFilter(params, subscriptions)
        serializer = ModelSubscriptionSerializer(filtered_subs.qs, many=True)
        return Response({"subscripciones":serializer.data})

#### Classes for payments related methods ###
class UserInvoices(APIView):
    """
    View to get the information about the payments and invoices of a 
    certain user
    *Requires token authentication
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request, pk):
        params = self.request.query_params
        invoices = Invoice.objects.filter(user=pk)
        filtered_inv = InvoiceFilter(params, invoices)
        serializer = ModelInvoiceSerializer(filtered_inv.qs, many=True)
        return Response({"invoices":serializer.data})

class AllInvoices(APIView):
    """
    View to get the information about all the payments
    *Requires token authentication
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request):
        params = self.request.query_params
        invoices = Invoice.objects.all()
        filtered_inv = InvoiceFilter(params, invoices)
        serializer = ModelInvoiceSerializer(filtered_inv.qs, many=True)
        return Response({"invoices":serializer.data})

class getUpdatedProfiles(APIView):
    """
    View to get the updated users
    *Requires token authentication
    """
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request):
        params = params = self.request.query_params
        users = UserProfile.objects.all()
        filtered_users = UserProfileFilter(params, users)
        serializer = UserProfileSerializer(filtered_users.qs, many=True)
        return Response({"usuarios_act":serializer.data})

#View Objects
list_users = ListUsers.as_view()
create_user = CreateUser.as_view()
read_user = ReadUser.as_view()
update_user = UpdateUser.as_view()
delete_user = DeleteUser.as_view()
user_status = UserStatus.as_view()
user_subscriptions = UserSubscription.as_view()
all_subscriptions = AllSubscriptions.as_view()
user_invoices = UserInvoices.as_view()
all_invoices = AllInvoices.as_view()
get_updated_profiles = getUpdatedProfiles.as_view()