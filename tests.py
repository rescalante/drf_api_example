import unittest, json
from unittest.mock import Mock, patch

from accounts.models import UserProfile, Subscription, Plan, PricingPlan, Product
from payments.models import Invoice, PromotionalCode
from django.http import QueryDict
from django.contrib.auth.models import User
from django.core.urlresolvers import resolve, reverse
from django.test import TestCase
from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.test import APIClient, APITestCase, APIRequestFactory, force_authenticate
from rest_framework.authtoken.models import Token

from .serializers import UserSerializer, UserProfileSerializer, ModelSubscriptionSerializer, ModelInvoiceSerializer
from .views import list_users, create_user, read_user, update_user, delete_user, user_status, user_subscriptions, all_subscriptions, user_invoices, all_invoices
from .filters import SubscriptionFilter, InvoiceFilter

# Create your tests here.
class Tests(APITestCase):

    def setUp(self):
        #Client for the API from DRF
        self.client = APIClient()

        #Mocked Django Users
        self.user1 = User.objects.create_user(username = "user1", 
                                            password = "1234", 
                                            email = "test1@mail.com")
        self.token1 = Token.objects.create(user=self.user1)

        self.user2 = User.objects.create_user(username = "user2", 
                                            password = "4321", 
                                            email = "test2@mail.com")
        self.token2 = Token.objects.create(user=self.user2)

        self.user_create_valid = {'username':'user3', 'email':'test3@mail.com','password':'12345'}
        self.user_create_invalid = {'username':'', 'email':'test3@mail.com','password':''}
        self.user_update_valid = {'username':'user2', 'email':'testing@mail.com','password':'1234566'}
        self.user_update_invalid = {'username':'user55', 'email':'','password':'12345'}

        #Mocked user profiles
        self.up1 = UserProfile.objects.create(user=self.user1, is_freemium=True, mod_user=True, sim_user=True, exp_user=False, user_disabled=False)
        self.up2 = UserProfile.objects.create(user=self.user2, is_freemium=False, mod_user=True, sim_user=False, exp_user=True, user_disabled=False)
        
        #Mocked Subscriptions
        self.plan1 = Plan.objects.create(plan_type='plan1')
        self.plan2 = Plan.objects.create(plan_type='plan2')

        self.prod1 = Product.objects.create(name='Producto1')
        self.prod2 = Product.objects.create(name='Producto2')

        self.pricing_plan1 = PricingPlan.objects.create(code='code1', plan=self.plan1, product=self.prod1, price=1000)
        self.pricing_plan2 = PricingPlan.objects.create(code='code2', plan=self.plan2, product=self.prod2, price=2000)

        self.sub1 = Subscription.objects.create(user=self.up1, status='active', start_date='2019-01-01', end_date='2019-10-10', product_plan=self.pricing_plan1)
        self.sub2 = Subscription.objects.create(user=self.up1, status='canceled', start_date='2019-02-01', end_date='2019-11-10', product_plan=self.pricing_plan1)
        self.sub3 = Subscription.objects.create(user=self.up1, status='expired', start_date='2019-01-17', end_date='2019-01-27', product_plan=self.pricing_plan1)
        self.sub4 = Subscription.objects.create(user=self.up2, status='active', start_date='2019-01-11', end_date='2019-01-31', product_plan=self.pricing_plan2)
        self.sub5 = Subscription.objects.create(user=self.up2, status='canceled', start_date='2019-01-13', end_date='2019-01-21', product_plan=self.pricing_plan2)
        self.sub6 = Subscription.objects.create(user=self.up2, status='expired', start_date='2019-01-19', end_date='2019-10-21', product_plan=self.pricing_plan2)
    
        #Mocked Invoices
        self.inv1 = Invoice.objects.create(user=self.up1, status='approved', created_date='2018-12-18', invoice_number='0001', amount='15')
        self.inv2 = Invoice.objects.create(user=self.up1, status='rejected', created_date='2019-01-15', invoice_number='0001', amount='20')
        self.inv3 = Invoice.objects.create(user=self.up1, status='approved', created_date='2018-11-15', invoice_number='0001', amount='25')
        self.inv4 = Invoice.objects.create(user=self.up2, status='rejected', created_date='2019-01-01', invoice_number='0002', amount='30')
        self.inv5 = Invoice.objects.create(user=self.up2, status='approved', created_date='2018-12-01', invoice_number='0003', amount='35')
        self.inv6 = Invoice.objects.create(user=self.up2, status='rejected', created_date='2018-12-18', invoice_number='0004', amount='40')

    def test_list_users(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/list_users', 
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = list_users(request)

        #Expected data
        expected_data = User.objects.all()
        serialized_data = {"Usuarios": UserSerializer(expected_data, many=True).data}

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)

    def test_create_user(self):
        #Creating the request
        factory = APIRequestFactory()
        user = json.dumps(self.user_create_valid)
        request = factory.post('suite-api/v1/create_user', 
                                user, 
                                content_type='application/json',
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = create_user(request)

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_user(self):
        #Creating the request
        factory = APIRequestFactory()
        user = json.dumps(self.user_create_invalid)
        request = factory.post('suite-api/v1/create_user', 
                                user, 
                                content_type='application/json',
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = create_user(request)

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_read_user(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/read_user/', 
                                kwargs={'pk': self.user2.pk},
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = read_user(request, self.user2.pk)

        #Expected data
        expected_data = get_object_or_404(User.objects.all(), pk=self.user2.pk)
        serialized_data = {"Usuarios": UserSerializer(expected_data).data}

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)
    
    def test_read_invalid_user(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/read_user/', 
                                kwargs={'pk': '1000'},
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = read_user(request, 1000)

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_user(self):
        #Creating the request
        factory = APIRequestFactory()
        user = json.dumps(self.user_update_valid)
        request = factory.put('suite-api/v1/update_user/', 
                                user, 
                                kwargs={'pk': self.user2.pk}, 
                                content_type='application/json',
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = update_user(request, self.user2.pk)

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_invalid_user(self):
        #Creating the request
        factory = APIRequestFactory()
        user = json.dumps(self.user_update_invalid)
        request = factory.put('suite-api/v1/update_user/', 
                                user, 
                                kwargs={'pk': self.user2.pk},
                                content_type='application/json',
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = update_user(request, self.user2.pk)

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_user(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.delete('suite-api/v1/delete_user/',
                                    kwargs={'pk': self.user2.pk},
                                    HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = delete_user(request, self.user2.pk)

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
                    
    def test_delete_invalid_user(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.delete('suite-api/v1/delete_user/',
                                    kwargs={'pk': '20'},
                                    HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = delete_user(request, '20')

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
 

 #Tests for userprofiles and subscriptions
    def test_get_user_status(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/users/status/', 
                                kwargs={'pk': self.up1.pk},
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = user_status(request, self.up1.pk)

        expected_data = get_object_or_404(UserProfile.objects.all(), pk=self.up1.pk)
        serialized_data = {"Usuario": UserProfileSerializer(expected_data).data}

        #Assertions
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)

    def test_get_all_subscriptions(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/users/subscriptions/', 
                                {'status':['active', 'canceled'], 'from_date':['2019-01-01'], 'to_date':['2019-01-31']},
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = all_subscriptions(request)

        expected_data = SubscriptionFilter(request.GET, Subscription.objects.all())
        serialized_data = {"Subscripciones": ModelSubscriptionSerializer(expected_data.qs, many=True).data}

        #Assertions
        self.assertIsInstance(expected_data, SubscriptionFilter)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)

#Tests for Invoice related methods
    def test_get_user_invoices(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/users/{:d}/invoices/'.format(self.up1.pk),
                                {'pk': self.up1.pk, 'status':'approved', 'invoice_number': '0001'},
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = user_invoices(request, self.up1.pk)

        expected_data = InvoiceFilter(request.GET, Invoice.objects.filter(user=self.up1.pk))
        serialized_data = {"Invoices": ModelInvoiceSerializer(expected_data.qs, many=True).data}

        #Assertions
        self.assertIsInstance(expected_data, InvoiceFilter)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)

    def test_get_all_invoices(self):
        #Creating the request
        factory = APIRequestFactory()
        request = factory.get('suite-api/v1/users/invoices/',
                                {'status':'approved', 'from_date': '2018-12-18', 'to_date':'2019-01-17'},
                                HTTP_AUTHORIZATION='Token ' + self.token1.key)
        response = all_invoices(request)

        expected_data = InvoiceFilter(request.GET, Invoice.objects.all())
        serialized_data = {"Invoices": ModelInvoiceSerializer(expected_data.qs, many=True).data}

        #Assertions
        self.assertIsInstance(expected_data, InvoiceFilter)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_data)
