from django.apps import AppConfig


class SuiteApiConfig(AppConfig):
    name = 'suite_api'
