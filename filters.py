import django_filters
from accounts.models import Subscription, UserProfile
from payments.models import Invoice

class SubscriptionFilter(django_filters.FilterSet):
    from_date = django_filters.DateFilter(name="start_date", lookup_expr='gte')
    to_date = django_filters.DateFilter(name="end_date", lookup_expr='lte')
    status = django_filters.ModelMultipleChoiceFilter(to_field_name='status', lookup_expr='in', queryset=Subscription.objects.all())
    updated_at = from_date = django_filters.DateFilter(name="last_updated_at", lookup_expr='gte')

    class Meta:
        model = Subscription
        fields = ['user__exp_user', 'status', 'start_date', 'end_date',]

class InvoiceFilter(django_filters.FilterSet):
    from_date = django_filters.DateFilter(name="created_date", lookup_expr='gte')
    to_date = django_filters.DateFilter(name="created_date", lookup_expr='lte')
    status = django_filters.ModelMultipleChoiceFilter(to_field_name='status', lookup_expr='in', queryset=Invoice.objects.all())

    class Meta:
        model = Invoice
        fields = ['user', 'status', 'created_date', 'invoice_number']

class UserProfileFilter(django_filters.FilterSet):
    updated_at = django_filters.DateFilter(name="last_updated_at", lookup_expr='gte')

    class Meta:
        model = UserProfile
        fields = ['id', 'exp_user', 'mod_user', 'sim_user', 'is_freemium', 'last_updated_at']