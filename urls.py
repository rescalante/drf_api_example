# coding=utf-8
from django.conf.urls import url, include

from rest_framework.authtoken.views import obtain_auth_token 
from .views import list_users, create_user, read_user, update_user, delete_user, user_status, user_subscriptions, all_subscriptions, user_invoices, all_invoices, get_updated_profiles
app_name = 'suite_api'

#Defining URL Patterns
urlpatterns = [
    url(r'^v1/auth$', obtain_auth_token, name='token_auth'),
    url(r'^v1/list_users$', list_users, name='api-list_users'),
    url(r'^v1/create_user$', create_user, name='api-create_user'),
    url(r'^v1/read_user/(?P<pk>[0-9]+)$', read_user, name='api-read_user'),
    url(r'^v1/update_user/(?P<pk>[0-9]+)$', update_user, name='api-update_user'),
    url(r'^v1/delete_user/(?P<pk>[0-9]+)$', delete_user, name='api-delete_user'),
    url(r'^v1/users/(?P<pk>[0-9]+)/status/$', user_status, name='api-user-status'),
    url(r'^v1/users/(?P<pk>[0-9]+)/subscriptions/$', user_subscriptions, name='api-user-subscriptions'),
    url(r'^v1/users/subscriptions$', all_subscriptions, name='api-all-subscriptions'),
    url(r'^v1/users/(?P<pk>[0-9]+)/invoices/$', user_invoices, name='api-user-invoices'),
    url(r'^v1/users/invoices$', all_invoices, name='api-all-invoices'),
    url(r'^v1/users/updates$', get_updated_profiles, name='api-get-updates')
]   